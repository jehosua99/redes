#ifndef REDES_H
#define REDES_H
#include <map>
#include <vector>
#include <string>

using namespace std;

class redes
{
private:
    char name;
    int valor;
    string archivoAle;
public:
    redes();
    string aleatorio();
    void nuevoEnrutador(map<char, map<char,int>>&tablaNodos);
    void eliminarEnrutador(map<char, map<char,int>>&tablaNodos);
    void imprimirTablaNodos(map<char, map<char,int>>&tablaNodos);
    void llenarTablaNodos(map<char, map<char,int>>&tablaNodos);
    void caminos(char inicio, char final, map<char, map<char,int>>&tablaNodos);
    void nuevoarchivo(map<char, map<char,int>>&tablaNodos);
};

#endif // REDES_H
