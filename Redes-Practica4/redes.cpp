#include "redes.h"
#include <fstream>
#include <map>
#include <string>
#include <vector>
#include <time.h>
#include <algorithm>
#include <iostream>

using namespace std;

redes::redes(){};

string redes::aleatorio()
{
    srand(time(NULL));
    auto s = to_string(1+rand()%4); //Convierto el numero generado a string
    archivoAle += s; //Agrego el numero al nombre del archivo
    archivoAle += ".txt";
    return archivoAle;
}

void redes::nuevoEnrutador(map<char, map<char,int>>&tablaNodos)  //Metodo para la creacion de un nuevo enrutador
{
    map <char,int> tabla; //Seran los enrutadores de conexion con sus costos
    cout << "Ingrese el nombre del nuevo enrutador (de una sola letra)\n";  //esta observacion es para trabajar con char
    cin >> name;
    ofstream archivo(archivoAle, ios::app);  //App para agregar contenido al archivo txt
    tablaNodos.clear(); //Es vaciada para volverla a llenar con los nuevos valores
    ifstream archivoRead(archivoAle);
    char word = '\0', auxWord = '\0', enrutador = '\0';  //word y auxword guardaran distintos caracteres que serviran para diferenciar enrutadores principales de conexiones o valores
    int valorConex = 0; //Guardara el valor de la conexion
    if (archivoRead.is_open())
    {
        char c;
        while (archivoRead.get(c)) //ciclo para obtener cada caracter del archivo
        {
            if (c != ',' && c != ' ' && c != '\n'){word += c;} //Si cumple la condicion se guardara en el string word todo lo anterior del texto, word se vaciara frecuentemente segun sea necesario
            else if (c == ' ')
            {
                auxWord = word;  //Guardo todo lo que contenia word en el auxiliar puesto que sera vaciado
                word = '\0';
            }
            else if (c == ',')
            {
                valorConex = word - 48;
                word = '\0'; //Se vacia word
                tabla.emplace(auxWord,valorConex); //Se guarda en el mapa el nombre del enrutador y el peso de la conexion
            }
            else if(c == '\n')
            {
                cout << "Ingrese el peso de la conexion con el router " << word << ", si no hay conexion escriba 0\n";  //Conforme el ciclo se desarrolla la lista cambia por tanto se podra indicar el nombre del enrutador
                cin >> valor;
                if (valor != 0) //0 Es el valor por defecto cuando no hay conexion entre enrutadores
                {
                    tabla.emplace(name, valor); //Si no es 0 lo agrego a la tabla
                    archivo << word << " " << valor << ","; //Guardo en el archivo de texto
                }
                enrutador = word;  //Cuando haya un salto de linea quiere decir que se termina la tabla del enrutador y se guarda su nombre para ser envaido al txt
                word = '\0';
                tablaNodos.emplace(enrutador,tabla); //Se guarda como llave del mapa el nombre del enrutador y su contenido sera un mapa con las conexiones, este mapa ya fue creado mas arriba
                tabla.clear();  //Se vacia tabla para poder albergar las nuevas conexiones
            }
        }
    }
    archivo << name << "\n";  //Al final se agrega el nombre del nuevo enrutador junto con un salto de linea
    archivo.close();
    archivoRead.close();
}

void redes::eliminarEnrutador(map<char, map<char,int>>&tablaNodos)
{
    cout << "Ingrese el nombre del enrutador a eliminar\n";
    cin >> name;
    map <char,int> tabla;
    ofstream archivo(archivoAle, ios::app);  //App para agregar contenido al archivo txt
    tablaNodos.clear(); //Es vaciada para volverla a llenar con los nuevos valores
    ifstream archivoRead(archivoAle);
    char word = '\0', auxWord = '\0', enrutador = '\0';  //word y auxword guardaran distintos caracteres que serviran para diferenciar enrutadores principales de conexiones o valores
    int valorConex = 0; //Guardara el valor de la conexion
    if (archivoRead.is_open())
    {
        char c;
        while (archivoRead.get(c)) //ciclo para obtener cada caracter del archivo
        {
            if (c != ',' && c != ' ' && c != '\n'){word += c;} //Si cumple la condicion se guardara en el string word todo lo anterior del texto, word se vaciara frecuentemente segun sea necesario
            else if (c == ' ')
            {
                auxWord = word;  //Guardo todo lo que contenia word en el auxiliar puesto que sera vaciado
                word = '\0';
            }
            else if (c == ',')
            {
                valorConex = word - 48;
                word = '\0'; //Se vacia word
                if (auxWord != name)
                    tabla.emplace(auxWord,valorConex); //Se guarda en el mapa el nombre del enrutador y el peso de la conexion
            }
            else if(c == '\n')
            {
                if (word != name) //El nombre del enrutador debera ser distinto al que queremos eliminar para poder guardar sus datos, de lo contrario no seran tomados en cuenta
                {
                    enrutador = word;  //Cuando haya un salto de linea quiere decir que se termina la tabla del enrutador y se guarda su nombre para ser envaido al txt
                    tablaNodos.emplace(enrutador,tabla); //Se guarda como llave del mapa el nombre del enrutador y su contenido sera un mapa con las conexiones, este mapa ya fue creado mas arriba
                }
                word = '\0';
                tabla.clear();  //Se vacia tabla para poder albergar las nuevas conexiones
            }
        }
    }
    archivo.close();
    archivoRead.close();
}

void redes::llenarTablaNodos(map<char, map<char,int>>&tablaNodos)
{
    ifstream archivo(archivoAle);
    map <char,int> tabla;
    char word = '\0', auxWord = '\0', enrutador = '\0';  //word y auxword guardaran distintos caracteres que serviran para diferenciar enrutadores principales de conexiones o valores
    int valorConex = 0; //Guardara el valor de la conexion
    if (archivo.is_open())
    {
        while (archivo.good())
        {
            char c;
            while (archivo.get(c)){ //ciclo para obtener cada caracter del archivo
                if (c != ',' && c != ' ' && c != '\n'){word += c;} //Si cumple la condicion se guardara en el string word todo lo anterior del texto, word se vaciara frecuentemente segun sea necesario
                else if (c == ' ')
                {
                    auxWord = word;  //Guardo todo lo que contenia word en el auxiliar puesto que sera vaciado
                    word = '\0';
                }
                else if (c == ',')
                {
                    valorConex = word - 48;
                    word = '\0'; //Se vacia word
                    tabla.emplace(auxWord,valorConex); //Se guarda en el mapa el nombre del enrutador y el peso de la conexion
                }
                else if(c == '\n')
                {
                    enrutador = word;  //Cuando haya un salto de linea quiere decir que se termina la tabla del enrutador y se guarda su nombre para ser envaido al txt
                    word = '\0';
                    tablaNodos.emplace(enrutador,tabla); //Se guarda como llave del mapa el nombre del enrutador y su contenido sera un mapa con las conexiones, este mapa ya fue creado mas arriba
                    tabla.clear();  //Se vacia tabla para poder albergar las nuevas conexiones
                }
            }
        }
    }
    archivo.close();
}

void redes::imprimirTablaNodos(map<char, map<char,int>>&tablaNodos)
{
    map<char, map<char,int>>::iterator i; //Se crea un iterador para el mapa de mapas
    map <char,int>:: iterator j; //Un iterador para el mapa que esta dentro del otro mapa
    for (i = tablaNodos.begin(); i != tablaNodos.end(); i++)  //Ciclo que recorrera el mapa de mapas
    {
        cout <<i->first<<":"; //Se imprime el nombre del enrutador principal junto con dos puntos
        for (j = i->second.begin(); j != i->second.end(); j++) //Ciclo que recorrera el mapa interno
        {
            cout <<j->first<<"="; //Se imprime el enrutador con el cual se esta conectando
            cout <<j->second<<" "; //Se imprime el valor de la conexion
        }
        cout <<endl; //Cuando termina el ciclo interno se debera saltar de linea para diferenciar enrutadores
    }
}

/*El siguiente metodo es una modificacion del algoritmo de dijkstra
tomado de la pagina https://gist.github.com/vertexclique/7410577, las funciones modificadas estaran comentadas*/
void redes::caminos(char inicio, char final, map<char, map<char,int>>&tablaNodos)
{
    int suma = 0, tamano = 0, conta = 0; //Guardara el costo del envio
    map <char, int> distancia;
    map <char, char> previo;
    vector<char> nodos;
    vector<char> camino;
    auto comparator = [&] (char left, char right) //Tomara los valores por referencia y sera usado mas adelante para ordenar datos
    { return distancia[left] > distancia[right]; };
    for (auto &vertex : tablaNodos) //Guardara el mapa en una variable por referencia para recorrerla
    {
        if (vertex.first == inicio) // Verifica si el enrutador de salida es el primero
            distancia[vertex.first] = 0; //Si es asi guardara como distancia 0
        else
            distancia[vertex.first] = 999; //De lo contrario se pone un valor cualquiera grande
        nodos.push_back(vertex.first); //Ingresa a nodos, que contendra el nombre de todos los enrutadores
        push_heap(begin(nodos), end(nodos), comparator);  //Ordena los enrutadores
    }
    while(!nodos.empty()) //Mientras nodo no este vacio
    {
        pop_heap(begin(nodos), end(nodos), comparator);
        char smallest = nodos.back(); //Accede al ultimo elemento de nodos
        nodos.pop_back();
        if (smallest == final) //Mira si el nodo accedido es al que se quiere llegar
        {
            while (previo.find(smallest) != end(previo)) //Mientras el enrutador de menor costo no este al final ira guardando el camino
            {
                camino.push_back(smallest); //Guarda el nombre del enrutador con menos costo
                smallest = previo[smallest]; //Conserva el siguiente enrutador a guardar
            }
            break;
        }
        if (distancia[smallest] == 999) //Si no inicia desde ahi seguira buscando hasta que llegue al nodo deseado
            break;
        for (auto& neighbor : tablaNodos[smallest]) //busca en los enrutadores con conexion
        {
            int alt = distancia[smallest] + neighbor.second; //Saca el valor de la conexion
            if (alt < distancia[neighbor.first]) //Si la distancia previamente guardada es mayor sera reemplazada
            {
                distancia[neighbor.first] = alt; //Se reemplaza
                previo[neighbor.first] = smallest; //Enrutadores ya buscados
                make_heap(begin(nodos), end(nodos), comparator); //Encentra la siguiente conexion
            }
        }
    }
    camino.push_back(inicio); //Como el enrutador de salida no es guardado es agregado al final del vector
    map<char, map<char,int>>::iterator i;
    map <char,int>:: iterator j;
    tamano = camino.size(); //Guardo el tamaño del vector puesto que necesito que imprima esa cantidad de veces
    cout << "El camino es:\n";
    for (i = tablaNodos.begin(); i != tablaNodos.end(); i++) //Recorro el mapa de mapas
    {
        if (camino.back() == i->first) //Saco el ultimo enrutador guardado en camino y lo busco en el mapa
        {
            conta ++; //Contara cuantas veces se cumple la condicion
            cout << camino.back() << " "; //Imprimo el camino
            camino.pop_back(); //Elimino el ultimo enrutador del vector para iterar sobre los siguientes
            for (j = i->second.begin(); j != i->second.end(); j++) //Busco en el mapa de conexinoes
            {
                if (camino.back() == j->first) //Busco el ultimo enrutador del vector (que seria el siguiente del camino a seguir) en el mapa
                {
                    if (!camino.empty()) //Si el camino no esta vacio acceso al costo de su conexion y la sumo
                        suma += j->second;
                }
            }
        }
    }
    if (conta != tamano) //Si son distintas es porque paso al enrutador final por tanto lo imprimo
        cout << final << " ";
    cout << "\nEl costo del envio es: " << suma << endl; //imprimo el costo del envio
}

void redes::nuevoarchivo(map<char, map<char, int> > &tablaNodos)
{
    char principal, conexion;
    int valor;
    ofstream archivo (archivoAle);
    map<char, map<char,int>>::iterator i; //Se crea un iterador para el mapa de mapas
    map <char,int>:: iterator j; //Un iterador para el mapa que esta dentro del otro mapa
    for (i = tablaNodos.begin(); i != tablaNodos.end(); i++)  //Ciclo que recorrera el mapa de mapas
    {
        principal = i->first;
        for (j = i->second.begin(); j != i->second.end(); j++) //Ciclo que recorrera el mapa interno
        {
            conexion = j->first; //Se imprime el enrutador con el cual se esta conectando
            valor = j->second; //Se imprime el valor de la conexion
            archivo << conexion << " " << valor << ",";
        }
        archivo << principal << "\n";
    }
    archivo.close();
}
