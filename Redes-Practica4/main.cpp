#include <iostream>
#include "redes.h"
#include <fstream>
#include <map>

using namespace std;

int main()
{
    int opcion;
    char flag = 'y', salida, llegada;  //Se crean variables que serviran para saber qué desea hacer el usuario
    map<char, map<char,int>>tablaNodos; // se almacenaran los datos de los enrutadores siendo la llave del primer mapa el enrutador, y la llave del segundo mapa el enrutador con el cual se conecta junto con el valor de su conexion
    map <char,int> tabla; //Contiene el enrutador de conexion y su valor
    redes red;  //Creo un objeto de la clase redes
    red.aleatorio(); //Genero un numero aleatorio que determinara el archivo a abrir
    cout << "los enrutadores existentes con sus respectivas conexiones son:\n";
    red.llenarTablaNodos(tablaNodos);  //Relleno la tabla con el metodo de la clase
    red.imprimirTablaNodos(tablaNodos); //Imprimo la tabla para que el usuario sepa con que se esta trabajando
    while (flag == 'y' || flag == 'Y'){
        cout << "Seleccione una opcion:\n[1] Agregar un nuevo enrutador\n[2] Eliminar un enrutador\n[3] Conocer el valor de envio de un enrutador a otro con su camino\n";
        cin >> opcion;
        switch (opcion){
        case 1:
            red.nuevoEnrutador(tablaNodos);  //Llamo el metodo para crear un nuevo enrutador
            cout << "Su nueva tabla de enrutadores es:\n";
            red.llenarTablaNodos(tablaNodos);  //Actualizo la tabla de enrutadores
            red.imprimirTablaNodos(tablaNodos); //Imprimo la tabla actualizada
            red.nuevoarchivo(tablaNodos);
            break;
        case 2:
            red.eliminarEnrutador(tablaNodos);
            cout << "Su nueva tabla de enrutadores es:\n";
            red.imprimirTablaNodos(tablaNodos);
            break;
        case 3:
            cout << "Ingrese el enrutador de salida (El programa reconoce mayusculas y minusculas asegurese de escribir correctamente los nombres)\n";
            cin >> salida;
            cout << "Ingrese el enrutador de llegada\n";
            cin >> llegada;
            red.caminos(salida, llegada, tablaNodos); //Llamo al metodo para encontrar el camino mas corto
            break;
        default:
            cout << "No ha ingresado una opcion correcta\n";}
        cout << "Desea realizar una nueva accion? (y/n)\n"; //Pregunto si desea hacer una accion mas
        cin >> flag;
    }
    return 0;
}
